package jpa.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "comment")
public class Comment {
    private int commentId;
    private String commentText;
    private Timestamp commentCreated;
    private Timestamp commentUpdated;
    private Post post;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "comment_id")
    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    @Column(name = "comment_text")
    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }


    @Column(name = "comment_created")
    public Timestamp getCommentCreated() {
        return commentCreated;
    }

    public void setCommentCreated(Timestamp commentCreated) {
        this.commentCreated = commentCreated;
    }


    @Column(name = "comment_updated")
    public Timestamp getCommentUpdated() {
        return commentUpdated;
    }

    public void setCommentUpdated(Timestamp commentUpdated) {
        this.commentUpdated = commentUpdated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Comment comment = (Comment) o;

        if (commentId != comment.commentId) return false;
        if (commentCreated != null ? !commentCreated.equals(comment.commentCreated) : comment.commentCreated != null)
            return false;
        if (commentText != null ? !commentText.equals(comment.commentText) : comment.commentText != null) return false;
        if (commentUpdated != null ? !commentUpdated.equals(comment.commentUpdated) : comment.commentUpdated != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = commentId;
        result = 31 * result + (commentText != null ? commentText.hashCode() : 0);
        result = 31 * result + (commentCreated != null ? commentCreated.hashCode() : 0);
        result = 31 * result + (commentUpdated != null ? commentUpdated.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "post_id", referencedColumnName = "post_id", nullable = false)
    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    @Override
    public String toString() {
        return commentCreated.toString();
    }
}
