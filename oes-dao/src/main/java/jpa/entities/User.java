package jpa.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "user")
public class User {
    private int userId;
    private String userName;
    private Timestamp userCreated;
    private Timestamp userUpdated;
    private Collection<Post> posts;

    @Id
    @Column(name = "user_id")
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }


    @Column(name = "user_created")
    public Timestamp getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(Timestamp userCreated) {
        this.userCreated = userCreated;
    }


    @Column(name = "user_updated")
    public Timestamp getUserUpdated() {
        return userUpdated;
    }

    public void setUserUpdated(Timestamp userUpdated) {
        this.userUpdated = userUpdated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (userId != user.userId) return false;
        if (userCreated != null ? !userCreated.equals(user.userCreated) : user.userCreated != null) return false;
        if (userName != null ? !userName.equals(user.userName) : user.userName != null) return false;
        if (userUpdated != null ? !userUpdated.equals(user.userUpdated) : user.userUpdated != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        result = 31 * result + (userCreated != null ? userCreated.hashCode() : 0);
        result = 31 * result + (userUpdated != null ? userUpdated.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "user")
    public Collection<Post> getPosts() {
        return posts;
    }

    public void setPosts(Collection<Post> posts) {
        this.posts = posts;
    }

    @Override
    public String toString() {
        return userName;
    }
}
