package jpa.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@NamedQueries(
        { @NamedQuery(name = "Post.GetAllPostsId", query = "SELECT p.postId FROM Post p")}
)
@Table(name = "post")
public class Post {
    private int postId;
    private String postTitle;
    private String postText;
    private Timestamp postCreated;
    private Timestamp postUpdated;
    private Collection<Comment> comments;
    private User user;

    @Id
    @Column(name = "post_id")
    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }


    @Column(name = "post_title")
    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }


    @Column(name = "post_text")
    public String getPostText() {
        return postText;
    }

    public void setPostText(String postText) {
        this.postText = postText;
    }


    @Column(name = "post_created")
    public Timestamp getPostCreated() {
        return postCreated;
    }

    public void setPostCreated(Timestamp postCreated) {
        this.postCreated = postCreated;
    }


    @Column(name = "post_updated")
    public Timestamp getPostUpdated() {
        return postUpdated;
    }

    public void setPostUpdated(Timestamp postUpdated) {
        this.postUpdated = postUpdated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Post post = (Post) o;

        if (postId != post.postId) return false;
        if (postCreated != null ? !postCreated.equals(post.postCreated) : post.postCreated != null) return false;
        if (postText != null ? !postText.equals(post.postText) : post.postText != null) return false;
        if (postTitle != null ? !postTitle.equals(post.postTitle) : post.postTitle != null) return false;
        if (postUpdated != null ? !postUpdated.equals(post.postUpdated) : post.postUpdated != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = postId;
        result = 31 * result + (postTitle != null ? postTitle.hashCode() : 0);
        result = 31 * result + (postText != null ? postText.hashCode() : 0);
        result = 31 * result + (postCreated != null ? postCreated.hashCode() : 0);
        result = 31 * result + (postUpdated != null ? postUpdated.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "post")
    public Collection<Comment> getComments() {
        return comments;
    }

    public void setComments(Collection<Comment> comments) {
        this.comments = comments;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return postTitle;
    }
}
