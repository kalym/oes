package jpa.dao;

import jpa.entities.Post;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless(name="postDAO")

public class PostDAO extends BaseDaoImpl<Post,Integer> {

    @PersistenceContext(unitName="JPADemoPU")
    private EntityManager em;

    public PostDAO() {
        super(Post.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    public List<Integer> getAllPostsId(){
        return (List<Integer>) em.createNamedQuery("Post.GetAllPostsId")
                .getResultList();
    }
}
