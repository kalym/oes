package jpa.dao;

import jpa.entities.Comment;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class CommentDAO extends BaseDaoImpl<Comment,Integer> {

    @PersistenceContext(unitName="JPADemoPU")
    private EntityManager em;

    public CommentDAO() {
        super(Comment.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
