package jpa.dao;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;


public abstract class BaseDaoImpl<T, E> implements Dao<T, E> {
    
    
    protected Class<T> entityClass;

    protected abstract EntityManager getEntityManager();
    
    public BaseDaoImpl() {
    }
    
    public BaseDaoImpl(Class type){
        this.entityClass = type;
    }

    
    
    @Override
    public void createItem(T item) {
        getEntityManager().persist(item);
    }

    @Override
    public void updateItem(T item) {
        getEntityManager().merge(item);
    }

    @Override
    public void deleteItem(T item) {
        getEntityManager().remove(item);
    }

    @Override
    public T readItem(E id) {
        return getEntityManager().find(entityClass, id);
    }

    @Override
    public List<T> getAllItems() {
        String queryString = new StringBuilder().append("SELECT e FROM ")
                .append(entityClass.getSimpleName())
                .append(" e").toString();
        Query getAllItemsQuery  = getEntityManager().createQuery(queryString);
        return (List<T> )getAllItemsQuery.getResultList();
    }
}
