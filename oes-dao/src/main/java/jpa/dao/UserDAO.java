package jpa.dao;

import jpa.entities.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class UserDAO extends BaseDaoImpl<User,Integer> {

    @PersistenceContext(unitName="JPADemoPU")
    private EntityManager em;

    public UserDAO() {
        super(User.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
