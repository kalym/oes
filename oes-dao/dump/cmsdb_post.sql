CREATE DATABASE  IF NOT EXISTS `cmsdb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `cmsdb`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: cmsdb
-- ------------------------------------------------------
-- Server version	5.6.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post` (
  `post_id` int(11) NOT NULL,
  `post_created` datetime DEFAULT NULL,
  `post_text` varchar(10000) DEFAULT NULL,
  `post_title` varchar(255) DEFAULT NULL,
  `post_updated` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`post_id`),
  KEY `FK3498A031476B34` (`user_id`),
  CONSTRAINT `FK3498A031476B34` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` VALUES (1,'2014-12-12 00:00:00','Hello this is first post','first post','2014-12-12 00:00:00',1),(2,'2014-12-12 00:00:00','another kind of shit','yep)','2014-12-12 00:00:00',1),(3,'2014-12-12 00:00:00','                <p> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,\n                    totam\n                    rem\n                    aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt\n                    explicabo.\n                    Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur\n                    magni\n                    dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia\n                    dolor\n                    sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et\n                    dolore\n                    magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam\n                    corporis\n                    suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure\n                    reprehenderit\n                    qui in\n                    ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo\n                    voluptas\n                    nulla pariatur?\"</p>\n\n                <p> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,\n                    totam\n                    rem\n                    aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt\n                    explicabo.\n                    Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur\n                    magni\n                    dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia\n                    dolor\n                    sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et\n                    dolore\n                    magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam\n                    corporis\n                    suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure\n                    reprehenderit\n                    qui in\n                    ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo\n                    voluptas\n                    nulla pariatur?\"</p>\n\n                <p> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,\n                    totam\n                    rem\n                    aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt\n                    explicabo.\n                    Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur\n                    magni\n                    dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia\n                    dolor\n                    sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et\n                    dolore\n                    magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam\n                    corporis\n                    suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure\n                    reprehenderit\n                    qui in\n                    ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo\n                    voluptas\n                    nulla pariatur?\"</p>','kyky','2014-12-12 00:00:00',1);
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-10-09 15:34:02
